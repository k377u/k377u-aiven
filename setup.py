from setuptools import setup, find_packages


setup(
    version="0.1",
    packages=find_packages(),
    install_requires=[
        "psycopg2",
        "kafka-python"
    ],
    extras_require={
        "dev": [
            "pytest",
            "pytest-cov",
            "pytest-html",
            "pycodestyle",
            "flake8",
        ]
    },
    entry_points={
        "console_scripts": [
            "producer = k377u_aiven.commands.producer:main",
            "consumer = k377u_aiven.commands.consumer:main",
            "create_topic = k377u_aiven.commands.create_topic:main",
            "delete_topic = k377u_aiven.commands.delete_topic:main",
            "purge_table = k377u_aiven.commands.purge_table:main",
            "create_table = k377u_aiven.commands.create_table:main",
            "print_table = k377u_aiven.commands.print_table:main"
        ]
    },
    tests_require=["k377u_aiven[dev]"],
    name="k377u_aiven",
    python_requires=">=3.6",
    license="MIT License 2019 K377U",
    author="Roope Tikkanen",
    author_email="roope@k377u.dev",
    maintainer="Roope Tikkanen",
    maintainer_email="roope@k377u.dev",
    keywords="aiven",
    description="",
    long_description="",
    classifiers=[
        "License :: OSI Approved :: MIT License 2019 K377U",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.6"
    ]
)
