ENV_NAME?=env
PYTHON?=python3

env-test:
	@if ! test -d $(ENV_NAME) ; \
	then echo "No virtualenv '$(ENV_NAME)' found"; \
	exit 1; \
	fi

env-build:
	@echo
	@test -d $(ENV_NAME) || virtualenv --python=$(PYTHON) $(ENV_NAME)
	@echo

env-upgrade: env-test
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		pip install pip setuptools wheel --upgrade ; \
	)
	@echo

install:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		pip install -e .[dev]; \
	)
	@echo

dev-env: env-build env-upgrade

dev: dev-env install
	@echo
	@echo "Activate virtualenv with:"
	@echo
	@echo ". $(ENV_NAME)/bin/activate"
	@echo

prepare:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		create_table; \
		create_topic; \
	)
	@echo

pep8:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		pycodestyle --config dev/pycodestyle k377u_aiven; \
	)
	@echo

test:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
		pytest \
			--html=test_result/report.html \
			--cov-config=dev/.coveragerc \
			--cov-report html:test_result \
			-v \
			--cov-report term-missing \
			--cov=k377u_aiven k377u_aiven/tests/; \
	)
	@echo

demo:
	@echo
	@( \
		. $(ENV_NAME)/bin/activate; \
    	purge_table; \
		echo "Hello\nworld\n!" | producer; \
		timeout 15 consumer; \
		print_table; \
	)
	@echo

clean:
	@echo
	-rm -fr .eggs/ .cache/ *.egg-info *.egg .pytest_cache/ test_result/ $(ENV_NAME)
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -type d -name "__pycache__" -delete
	-rm -f .coverage pylint.html 
	@echo
