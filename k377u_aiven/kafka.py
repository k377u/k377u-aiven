"""
Helpers for using kafka.

Found example how to configure kafka from here:
https://help.aiven.io/en/articles/489572-getting-started-with-aiven-kafka
"""
import os
from typing import Iterable
from contextlib import contextmanager
from kafka import KafkaConsumer, KafkaProducer
from kafka.admin import KafkaAdminClient, NewTopic
from kafka.errors import TopicAlreadyExistsError, UnknownTopicOrPartitionError
# import logging
# logging.basicConfig(level=logging.DEBUG)

KAFKA = os.environ.get('KAFKA')
KAFKA_TOPIC = os.environ.get('KAFKA_TOPIC')
KAFKA_GROUP = os.environ.get('KAFKA_GROUP')
KAFKA_SSL_CAFILE = os.environ.get('KAFKA_SSL_CAFILE')
KAFKA_SSL_CERTFILE = os.environ.get('KAFKA_SSL_CERTFILE')
KAFKA_SSL_KEYFILE = os.environ.get('KAFKA_SSL_KEYFILE')


@contextmanager
def get_producer() -> Iterable[KafkaProducer]:
    """
    KafkaProducer with configuration and wrapped so that it can
    be used with with syntax.
    """
    producer = KafkaProducer(
        bootstrap_servers=KAFKA,
        security_protocol="SSL",
        ssl_cafile=KAFKA_SSL_CAFILE,
        ssl_certfile=KAFKA_SSL_CERTFILE,
        ssl_keyfile=KAFKA_SSL_KEYFILE,
    )
    try:
        yield producer
    finally:
        producer.close()


@contextmanager
def get_consumer(topic_name: str = KAFKA_TOPIC) -> Iterable[KafkaConsumer]:
    """
    KafkaConsumer with configuration and wrapped so that it can
    be used with with syntax.

    We need to make sure that each message is actually stored in database.

    With configuration:

    auto_offset_reset='earliest'
    enable_auto_commit=False

    We make it so that consumer.commit() must be called to consume the message.

    This is done after we have succesful insert to postgres.
    """
    consumer = KafkaConsumer(
        topic_name,
        bootstrap_servers=KAFKA,
        auto_offset_reset='earliest',
        group_id=KAFKA_GROUP,
        enable_auto_commit=False,
        security_protocol="SSL",
        ssl_cafile=KAFKA_SSL_CAFILE,
        ssl_certfile=KAFKA_SSL_CERTFILE,
        ssl_keyfile=KAFKA_SSL_KEYFILE,

    )
    try:
        yield consumer
    finally:
        consumer.close()


def get_admin_client():
    """
    Get conigured KafkaAdminClient to be used for admin level tasks.

    Example for how to use KafkaAdminClient.

    https://stackoverflow.com/questions/26021541/how-to-programmatically-create-a-topic-in-apache-kafka-using-python
    """
    return KafkaAdminClient(
        bootstrap_servers=KAFKA,
        security_protocol="SSL",
        ssl_cafile=KAFKA_SSL_CAFILE,
        ssl_certfile=KAFKA_SSL_CERTFILE,
        ssl_keyfile=KAFKA_SSL_KEYFILE
    )


def create_topic(admin_client: KafkaAdminClient, topic_name: str = KAFKA_TOPIC) -> None:
    """
    Add topic to kafka. Repeated taks just tells that TOPIC_ALREADY_EXISTS.
    """
    try:
        admin_client.create_topics(
            new_topics=[
                NewTopic(
                    name=topic_name,
                    num_partitions=1,
                    replication_factor=1
                )
            ],
            validate_only=False
        )
    except TopicAlreadyExistsError as error:
        print(error.message)


def delete_topic(admin_client: KafkaAdminClient, topic_name: str = KAFKA_TOPIC) -> None:
    """
    Delete topic from kafka. Repeated call just tells that UNKNOWN_TOPIC_OR_PARTITION.
    """
    try:
        admin_client.delete_topics(topics=[topic_name])
    except UnknownTopicOrPartitionError as error:
        print(error.message)


def produce(producer: KafkaProducer, message: str, topic_name: str = KAFKA_TOPIC) -> None:
    """
    Produce one message to configured topic. Kafka eats messages in binary format.
    Message is encoded before sent.
    """
    producer.send(topic_name, str.encode(message))


def consume(consumer: KafkaConsumer) -> Iterable[str]:
    """
    Consume all messages from kafka topic. Messages are in binary format when they go
    through kafka. Decode message before yielding it.
    """
    for message in consumer:
        yield message.value.decode()
