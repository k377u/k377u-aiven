import os
import sys
from k377u_aiven import kafka


def main():
    try:
        with open(sys.stdin.fileno(), "r") as file_descriptor, kafka.get_producer() as producer:
            print(f"Start producing", file=sys.stderr)
            for i, row in enumerate(file_descriptor):
                message = row.rstrip()
                kafka.produce(producer, message)
                print(f"Produced {i+1}", end="\r", flush=True, file=sys.stderr)
            print("Producing done", file=sys.stderr)
    except KeyboardInterrupt:
        print("Producing stopped by user", file=sys.stderr)


if __name__ == "__main__":
    main()
