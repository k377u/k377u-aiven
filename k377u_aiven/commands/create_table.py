from k377u_aiven import postgres


def main():
    with postgres.connect() as connection:
        postgres.create_table(connection)


if __name__ == "__main__":
    main()
