import argparse
from k377u_aiven import kafka


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--topic-name",
        dest="topic_name",
        default=kafka.KAFKA_TOPIC
    )
    args = parser.parse_args()
    admin_client = kafka.get_admin_client()
    kafka.delete_topic(admin_client, args.topic_name)


if __name__ == "__main__":
    main()
