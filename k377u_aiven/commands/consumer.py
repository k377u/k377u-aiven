import os
import sys
from k377u_aiven import postgres, kafka


def main():
    try:
        with postgres.connect() as connection, kafka.get_consumer() as consumer:
            print(f"Start consuming", file=sys.stderr)
            for i, message in enumerate(kafka.consume(consumer)):
                postgres.insert(
                    connection=connection,
                    item=[message],
                    consumer=consumer
                )
                print(f"Consumed {i+1}", end="\r", flush=True, file=sys.stderr)
            print(f"Consuming done", file=sys.stderr)
    except KeyboardInterrupt:
        print(f"Consuming stopped by user", file=sys.stderr)


if __name__ == "__main__":
    main()
