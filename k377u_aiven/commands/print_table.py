from k377u_aiven import postgres


def main():
    with postgres.connect() as connection:
        for record in postgres.read_table(connection):
            print(record)


if __name__ == "__main__":
    main()
