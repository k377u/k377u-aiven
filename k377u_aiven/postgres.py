import os
import psycopg2
import psycopg2.extras
from kafka import KafkaConsumer


POSTGRES_USER = os.environ.get('POSTGRES_USER')
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
POSTGRES_HOST = os.environ.get('POSTGRES_HOST')
POSTGRES_PORT = os.environ.get('POSTGRES_PORT')
POSTGRES_DATABASE = os.environ.get('POSTGRES_DATABASE')
POSTGRES_TABLE_NAME = os.environ.get('POSTGRES_TABLE_NAME')


def connect():
    """
    Connect to Aiven postgres. Set autocommit True as there is no
    logic where it should be False.
    """
    connection = psycopg2.connect(
        user=POSTGRES_USER,
        password=POSTGRES_PASSWORD,
        host=POSTGRES_HOST,
        port=POSTGRES_PORT,
        database=POSTGRES_DATABASE,
        sslmode="require"
    )
    connection.autocommit = True
    return connection


CREATE_TABLE = """
CREATE TABLE IF NOT EXISTS {table_name} (
id serial PRIMARY KEY,
log TEXT
);
"""


def create_table(connection, table_name: str = POSTGRES_TABLE_NAME) -> None:
    """Create table in postgres."""
    cursor = connection.cursor()
    cursor.execute(CREATE_TABLE.format(table_name=table_name))


DROP_TABLE = """
DROP TABLE IF EXISTS {table_name};
"""


def delete_table(connection, table_name: str = POSTGRES_TABLE_NAME) -> None:
    """Drop table from postgres."""
    cursor = connection.cursor()
    cursor.execute(DROP_TABLE.format(table_name=table_name))


EMPTY_TABLE = """
TRUNCATE {table_name};
"""


def purge_table(connection, table_name: str = POSTGRES_TABLE_NAME) -> None:
    """Purge table content."""
    cursor = connection.cursor()
    cursor.execute(EMPTY_TABLE.format(table_name=table_name))


INSERT_INTO = """
INSERT INTO {table_name} (log) VALUES (%s);
"""


def insert(connection, item: str, consumer: KafkaConsumer, table_name: str = POSTGRES_TABLE_NAME) -> None:
    """
    Insert one item to database.

    We could add insert batch but that is not required now.

    Pass consumer for the function so that we can commit message as soon
    as we have insert it to database.

    """
    cursor = connection.cursor()
    cursor.execute(INSERT_INTO.format(table_name=table_name), item)
    consumer.commit()


SELECT_TABLE = """
SELECT * FROM {table_name} ORDER BY id;
"""


def read_table(connection, table_name: str = POSTGRES_TABLE_NAME) -> None:
    """Print table."""
    cursor = connection.cursor()
    cursor.execute(SELECT_TABLE.format(table_name=table_name))
    for record in cursor.fetchall():
        yield record
