"""
We should test more but for now just the happy success.

Failures and edge cases some other day.
"""
from k377u_aiven import postgres


class DummyConsumer():

    def commit(self):
        """
        Function insert does consumer.commit() so we need to mock it.
        """
        pass


def test_postgres():
    """
    Connect to postgres. Then delete test table, create new table,
    insert data to table, and query it.
    """
    table_name = "test_postgres"
    consumer = DummyConsumer()
    connection = postgres.connect()
    postgres.delete_table(connection, table_name)
    postgres.create_table(connection, table_name)
    postgres.insert(
        connection=connection,
        item=["hithere"],
        consumer=consumer,
        table_name=table_name
    )
    for record in postgres.read_table(
        connection=connection,
        table_name=table_name
    ):
        assert record == (1, "hithere")
