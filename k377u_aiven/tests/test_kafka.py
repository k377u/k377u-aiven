"""
Test kafka process.
"""
import pytest
from k377u_aiven import kafka


def test_kafka_process():
    """
    Send message to kafka and get it back. Message stays the same.
    """
    topic_name = "test_process"
    admin_client = kafka.get_admin_client()
    kafka.delete_topic(admin_client, topic_name)
    kafka.create_topic(admin_client, topic_name)
    with kafka.get_producer() as producer, kafka.get_consumer(topic_name) as consumer:
        message = "äö^+¨´\njee"
        kafka.produce(producer, message, topic_name)
        for msg in kafka.consume(consumer):
            assert message == msg
            break
    kafka.delete_topic(admin_client, topic_name)


def test_produce_binary_fail():
    """
    Function produce message must be string as it is decoded to binary.
    Fails with TypeError if binary.
    """
    with kafka.get_producer() as producer:
        with pytest.raises(TypeError):
            kafka.produce(producer, message=b"im binary")
