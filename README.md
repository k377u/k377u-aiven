# Aiven homework

This is a coding assignment for a backend developer position at Aiven.


# Configuration

Set these environment variables:

```
POSTGRES_USER
POSTGRES_PASSWORD
POSTGRES_HOST
POSTGRES_PORT
POSTGRES_DATABASE
POSTGRES_TABLE_NAME
KAFKA
KAFKA_SSL_CAFILE
KAFKA_SSL_CERTFILE
KAFKA_SSL_KEYFILE
KAFKA_GROUP
KAFKA_TOPIC
```

# How to dev


```
make dev
```

# Test

```
make pep8
make test
```

# Demo

Prepare database by running

```
create_table
```

and create kafka topic manually from https://console.aiven.io

OR

```
make prepare
```

Run short demo with:

```
make demo
```

This will remove all from postgres table, then produce text, run consumer for 15 seconds, and finally print data from postgres.

OR

To demo these features start 3 diffferent terminals and start `producer`, `consumer`, and `print_table` in each. Select system metric to read to kafka, for example `tail -f /var/log/syslog`.

# Scripts

## `producer`

Read stdin and produce its content to kafka topic. For example:

```tail -f /var/log/syslog | producer```


## `consumer`

Consume kafka topic and insert all messages to conigured postgres database.

## `create_topic`

Create kafka topic.

## `delete_topic`

Delete kafka topic.

## `create_table`

Create table in postgres where logs are stored.


## `print_table`

Print table content ordered by id.


## `purge_table`

Purge table content.
